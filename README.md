# UnityLayer

[![CI Status](https://img.shields.io/travis/Max Petrovsky/UnityLayer.svg?style=flat)](https://travis-ci.org/Max Petrovsky/UnityLayer)
[![Version](https://img.shields.io/cocoapods/v/UnityLayer.svg?style=flat)](https://cocoapods.org/pods/UnityLayer)
[![License](https://img.shields.io/cocoapods/l/UnityLayer.svg?style=flat)](https://cocoapods.org/pods/UnityLayer)
[![Platform](https://img.shields.io/cocoapods/p/UnityLayer.svg?style=flat)](https://cocoapods.org/pods/UnityLayer)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

UnityLayer is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'UnityLayer'
```

## Author

Max Petrovsky, maksym.petrovskyi@games24x7.com

## License

UnityLayer is available under the MIT license. See the LICENSE file for more info.

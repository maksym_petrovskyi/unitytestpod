@import Foundation;
@import UIKit;
@import UnityFramework;

@interface UnityWrapper: NSObject

+ (UnityFramework*) unityFrameworkLoad;
+ (void)runOnMainApp:(int)argc argv:(char* [])argv;
+ (void)runEmbeddedWithArgc:(int)argc
                       argv:(char* [])argv
              launchOptions:(NSDictionary<UIApplicationLaunchOptionsKey, id> *)appLaunchOpts;

@end

@import Foundation;
@import UnityFramework;
#import "UnityWrapper.h"

#define auto __auto_type

@implementation UnityWrapper

+ (NSBundle*) unityFrameworkBundle {
    NSString* bundlePath = nil;
    bundlePath = [NSBundle bundleForClass:UnityWrapper.class].bundlePath;
    bundlePath = [bundlePath stringByAppendingString: @"/Frameworks/UnityFramework.framework"];
    return [NSBundle bundleWithPath: bundlePath];
}

+ (UnityFramework*) unityFrameworkLoad {
    auto bundle = [self unityFrameworkBundle];
    if ([bundle isLoaded] == false) [bundle load];    
    UnityFramework* ufw = [bundle.principalClass getInstance];
    if (![ufw appController])
    {
        // unity is not initialized
        auto header = (MachHeader*)_MH_EXECUTE_SYM;
        [ufw setExecuteHeader:header];
    }
    return ufw;
}


+ (UnityFramework*)newFramework {
    UnityFramework* framework = self.unityFrameworkLoad;
    [framework setDataBundleId: self.unityFrameworkBundle.bundleIdentifier.UTF8String];
    return framework;
}

+ (void)runOnMainApp:(int)argc argv:(char* [])argv {
    [self.newFramework runUIApplicationMainWithArgc: argc argv: argv];
}

+ (void)runEmbeddedWithArgc:(int)argc argv:(char* [])argv launchOptions:(NSDictionary *)appLaunchOpts {
    [self.newFramework runEmbeddedWithArgc:argc argv:argv appLaunchOpts:appLaunchOpts];
}


@end



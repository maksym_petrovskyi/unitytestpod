#ifndef auto 
#define auto __auto_type
#endif

@implementation NSObject (Extensions)

+ (instancetype)cast:(id)object {
    return [object isKindOfClass:self] ? object : nil;
}

@end

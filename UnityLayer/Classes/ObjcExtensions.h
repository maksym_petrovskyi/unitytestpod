#ifndef auto 
#define auto __auto_type
#endif
@import Foundation;

@interface NSObject (Extensions)

+ (instancetype __nullable)cast:(id __nullable)object;

@end

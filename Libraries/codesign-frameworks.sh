#!/bin/sh

# WARNING: You may have to run Clean in Xcode after changing CODE_SIGN_IDENTITY! 

# Verify that $CODE_SIGN_IDENTITY is set
if [ -z "$CODE_SIGN_IDENTITY" ] ; then
    echo "CODE_SIGN_IDENTITY needs to be non-empty for codesigning frameworks!"

    if [ "$CONFIGURATION" = "Release" ] ; then
        exit 1
    else
        # Codesigning is optional for non-release builds.
        exit 0
    fi
fi

SAVEIFS=$IFS
IFS=$(echo -en "\n\b")

FRAMEWORK_DIR="${TARGET_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}"

# Loop through all frameworks
FRAMEWORKS=`find "${FRAMEWORK_DIR}" -type d -name "*.framework" | sed -e "s/\(.*\)/\1\/Versions\/A\//"`
RESULT=$?
if [[ $RESULT != 0 ]] ; then
    exit 1
fi

echo "Found:"
echo "${FRAMEWORKS}"

for FRAMEWORK in $FRAMEWORKS;
do
    echo "Signing '${FRAMEWORK}'"
    `codesign -f -v -s "${CODE_SIGN_IDENTITY}" "${FRAMEWORK}"`
    RESULT=$?
    if [[ $RESULT != 0 ]] ; then
        exit 1
    fi
done

# restore $IFS
IFS=$SAVEIFS

#Save it to a file in your project.
#Mine is called codesign-frameworks.sh.
#Add a “Run Script” build phase right after your “Copy Embedded Frameworks” build phase.
#You can call it “Codesign Embedded Frameworks”.
#Paste ./codesign-frameworks.sh (or whatever you called your script above) into the script editor text field.
#Build your app. All bundled frameworks will be codesigned.
#Updated 2012-11-14: Adding support for frameworks with special characters in their name (this does not include single quotes) to “codesign-frameworks.sh”.
#Updated 2013-01-30: Adding support for special characters in all paths (this should include single quotes) to “codesign-frameworks.sh”.
#Improvements welcome!
# from http://stackoverflow.com/questions/7697508/how-do-you-codesign-framework-bundles-for-the-mac-app-store


/*
 * Author: Sandeep.N.S.K
 */
// [!] important set UnityFramework in Target Membership for this file
// [!]           and set Public header visibility

#import <Foundation/Foundation.h>

// NativeCallsProtocol defines protocol with methods you want to be called from managed
@protocol NativeCallsProtocol
@required
- (void) showHostMainWindow:(NSString*)color;

//- (void) ConnectWS:(NSString*)strHostName withConnId:(NSString*)nConnID;
//- (void) ConnectWS:(NSString*)strHostName :(NSString*)nConnID ;
- (void) connectWS:(NSString*)strHostName :(NSNumber*)nConnID ;

- (void) DisConnectWs:(NSNumber*)nConnId;
- (void) SendWSMessage:(NSString*)strMsg :(NSNumber*)nConnId;
-(void) GetSessionIDFrmiOS:(NSString*)jaigu ;
-(void) getUserID;
- (void)createOverlay:(NSString*)action : (NSString*)url;
-(void) GetAddCashUrl;
-(void)isAddCashOnGameTableEnabled;
-(void) getAddCashGameTableUrl;
-(void) destroyOverlayIfExists;
-(void) getGameStartDetails;
- (void)sendFusionDataRequest:(NSString*)strPath : (NSNumber*)callbackId;
- (void)sendPostRequest:(NSString*)strPath :(NSString*)data : (NSNumber*)callbackId;
- (void)sendGetRequest:(NSString*)strPath : (NSNumber*)callbackId;
-(void) unityGameTableLoaded;


////////////////////////////////////(Start)Webview////////////////////////////////////////
- (void) createWebview_:(NSNumber*)widgetId :(NSString*)viewIdentifier;
- (void) destroyWebView:(NSNumber*)widgetId;
- (void) loadURL:(NSNumber*)widgetId :(NSString*)strURL;
- (void) setVisible:(NSNumber*)widgetId :(NSNumber*)isVisible;
- (void) setWebViewFrame:(NSNumber*)widgetId  :(NSNumber*)x :(NSNumber*)y :(NSNumber*)width :(NSNumber*)height :(NSNumber*)windowWidth :(NSNumber*)windowHeight;
-(void) testbool :(NSNumber*)boolval;
- (void) moveToanimate:(NSNumber*)widgetId  :(NSNumber*)duration :(NSNumber*)newX :(NSNumber*)newY :(NSNumber*)windowWidth :(NSNumber*)windowHeight;
- (void) scaleToanimate:(NSNumber*)widgetId  :(NSNumber*)duration :(NSNumber*)fromScale :(NSNumber*)toScale;
//- (void) widgetActionHandler:(NSString*)widget: (NSNumber*)actionType :(NSString*)jsonData;
- (void) evaluateJS:(NSNumber*)widgetId :(NSString*)js;
////////////////////////////////////(End)Webview////////////////////////////////////////


/////////////////////////////////FMGValidations///////////////////////////////////////


-(NSString*) getTournamentForId:(NSString*)trnID;
   -(NSString*) getFMGData;
   -(void) sendMttJoinWithdraw:(NSString*)action :(NSString*)tournamentID :(NSString*)joinBy;
   -(void) ShowWebView:(NSString*)withURl;
   -(BOOL) IsLocationBlocked;

- (void) SetLastPlayedGame:(NSNumber*)formatType  :(NSNumber*)noOfPlayer :(NSNumber*)tabId :(NSNumber*)entryFee :(NSNumber*)prizePerPoint;
   -(BOOL) validateGeoLocationAndShowDialog;
   -(void) ShowLobby:(NSString*)action;
   -(void) StayAwake:(NSString*)strStayAwake;
   -(NSString*) GetAppVersion;
   -(BOOL) IsConnected;
   -(BOOL) GetPracticeGamesBlockedData;
   -(BOOL) IsGPSEnabled;
   -(BOOL) IsMockLocationEnabled;
   -(BOOL) IsPrevLocationAvailable;
   -(BOOL) hasGPSPermission;
   -(BOOL) hasGPSTechError;
   -(BOOL) isLatLongToStateInprogress;
   -(int) GetAnalyticsBatchSize;
   -(int) GetAnalyticsInterval;
   -(NSString*) getMrcUrl;
   -(NSString*) getnucUrl;
   -(BOOL) isJoinMTTEnabled;
   -(BOOL) isBREforDeclarationEnable;
   -(NSString*) getGeoLocationState;
   -(BOOL) isNewTournamentAnimationEnabled;
   -(NSString*) getSpinWheelUrl;
   -(void) showRapToast:(NSString*)msg;
/////////////////////////////////FMGValidations (END)////////////////////////////////////////

-(void) sendClickStreamData :(NSString*)jsonstr;

   -(void) rapCreateLogger:(NSString*)loggerID;
   -(void) rapDisposeLogger:(NSString*)loggerID;
- (void) rapSendLogsToServer:(NSString*)loggerId  :(NSString*)timestamp :(NSString*)aaId :(NSString*)gameId :(NSString*)tournamentId;
-(BOOL) rapCheckIsLoggerAvailable;
-(NSString*) rapGetLogsBaseURL;



/////////////////////////WidrawNudge//Ashish
-(void) resultScreenCampaignShown:(NSString*)campaignId;
-(void) sendDataToNotifier: (NSString*)messageType :(NSString*)jsonStr;
//- (void) SendWSMessage:(NSString*)strMsg :(NSNumber*)nConnId;
////////////////////////////////////////////////////Rumble//Ashish




/////////////////////////////////PointsMTT (Start)////////////////////////////////////////
//func getPointsRummyTournmentData(byId trnId: NSNumber!) -> String! {
-(NSString*) getPointsRummyTournmentDataById :(NSNumber*)trnId NS_SWIFT_NAME(getPointsRummyTournmentData(byId trnId:)) ;
//-(NSString*) getPointsRummyTournmentDataById :(NSNumber*)trnId;
//getTournamentDetails(fromServer:)'
-(void) getTournamentDetailsFromServer:(NSNumber*)trnId NS_SWIFT_NAME(getTournamentDetails(fromServer:)) ;
//-(void) getTournamentDetailsFromServer:(NSNumber*)trnId ;
-(void) sendPointsMTTRebuyRequest:(NSNumber*)trntId: (NSNumber*)isAccepted;
-(void) getPointsTournmentMyJoinedGames;
-(void) getSeatSetupRanks:(NSNumber*)trntId:(NSNumber*)aaid ;
-(NSString*) getPointsMTTCurrentRound:(NSNumber*)trnId;
//-(void) sendMessageToLobbySocket :(NSString*)jsonstr;
-(void) sendMessageToLobbySocket :(NSString*)jsonstr NS_SWIFT_NAME(sendMessage(toLobbySocket jsonstr:)) ;
///__ObjC.NativeCallsProtocol:107:10: Protocol requires function 'sendMessage(toLobbySocket:)' with type '(String?) -> Void'
/////////////////////////////////PointsMTT (End)////////////////////////////////////////


////////////////////////////////////////////////////Rumble//Ashish
-(NSString*) getAllChildRumble:(NSString*) parentLBId;

-(NSString*) getJoinedData:(NSString*) lbId;

-(void) onBadgeClicked;
-(void) sendRumbleMessageOnSocket :(NSString*) rumblejson;

-(int) getBadgeCount:(NSString*)arrLbIds;
-(NSString*) getBadgeCountStr:(NSString*) arrLbIds;
-(NSString*) getJoinedAndRunningRumbleTemplateList;
 

-(NSString*) getLeaderboardResultWindowOptinUrl;
-(NSString*) getLeaderboardGTUrl;
-(NSString*) getLeaderboardLobbyUrl;

-(void) sendOptInRequest : (NSString*)initiationPoint:(NSString*)lbId;


-(NSString*) getRumbleData:(NSString*) lbID;

-(NSString*) getLeaderboardResultWindowOptinUrlList;

-(NSString*) getTrackingObject:(NSString*) leaderBoardId;

-(void) showLeaderboardHUD;

-(BOOL) isLeaderboardAvailableForJoin;

-(BOOL) isRunningChildHasTemplateInHUD:(NSString*) templateId;

-(BOOL) isUserOpted:(NSString*) templateId;

-(NSString*) getGameCardsToBeShownInGameTable;

///Rumble end //Ashish


// Custom Slider Methods
-(NSString*) getRecommendedGames;
-(void) updateIgnoreCount:(NSString*)recommendation;
-(BOOL) isGameShown:(NSNumber*)prizeType:(NSNumber*)gameType:(NSNumber*)player;
-(void) updateGameShown:(NSNumber*)prizeType :(NSNumber*)formatType :(NSNumber*)playerSize :(BOOL)value;
-(NSString*) getSavePlayedGameInfoUrl;
-(BOOL) shouldUpdateLastPlayed; // No need to use as the AB is concluded
-(NSString*) getUpdateLastPlayedUrl;

//-(void) getAddCashDefaultText;
//+ (NSString*)GetAddCashUrl;
//+ (BOOL)isAddCashOnGameTableEnabled;
//+ (NSString*) getAddCashGameTableUrl;

//+ (NSString*) getAddCashDefaultText;


// other methods

//+ (void)ConnectWS:(NSString*)strHostName withConnId:(NSNumber*)nConnId;
//
//+ (void)DisConnectWs:(NSNumber*)nConnId;
//
//+ (void)SendWSMessage:(NSString*)strMsg withConnId:(NSNumber*)nConnId;


@end

__attribute__ ((visibility("default")))
@interface FrameworkLibAPI : NSObject
// call it any time after UnityFrameworkLoad to set object implementing NativeCallsProtocol methods
+(void) registerAPIforNativeCalls:(id<NativeCallsProtocol>) aApi;

@end



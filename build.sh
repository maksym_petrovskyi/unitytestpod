CURRENT_DIR=$(pwd)
xcodebuild -project sources/iOSSimulator/Unity-iPhone.xcodeproj -scheme UnityFramework -configuration Release -sdk iphonesimulator CONFIGURATION_BUILD_DIR="$CURRENT_DIR/Libraries/iOSSimulator" CODE_SIGN_IDENTITY="" CODE_SIGNING_REQUIRED=NO clean build
xcodebuild -project sources/iOS/Unity-iPhone.xcodeproj -scheme UnityFramework -configuration Release -sdk iphoneos CONFIGURATION_BUILD_DIR="$CURRENT_DIR/Libraries/iOS" CODE_SIGN_IDENTITY="" CODE_SIGNING_REQUIRED=NO clean build

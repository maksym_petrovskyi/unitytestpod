if [[ ! -d "${CODESIGNING_FOLDER_PATH}/Frameworks/" ]]; then
    mkdir "${CODESIGNING_FOLDER_PATH}/Frameworks/"
fi

if [ ${EFFECTIVE_PLATFORM_NAME} == "-iphonesimulator" ]; then
  cp -r "${PODS_TARGET_SRCROOT}/Libraries/iOSSimulator/UnityFramework.framework" "${CODESIGNING_FOLDER_PATH}/Frameworks/UnityFramework.framework"
else
  cp -r "${PODS_TARGET_SRCROOT}/Libraries/iOS/UnityFramework.framework" "${CODESIGNING_FOLDER_PATH}/Frameworks/UnityFramework.framework"
fi

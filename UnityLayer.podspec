#
# Be sure to run `pod lib lint UnityLayer.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#
# def pod_path()
#   puts(Dir.pwd)
#   if File.exist?(Dir.pwd+"/Libraries") then
#     return Dir.pwd
#   else
#     return "${PODS_ROOT}/UnityLayer"
#   end
# end
if File.exist?(Dir.pwd+"../Podfile") then
  $UnityLibraryPath = Dir.pwd
else
  $UnityLibraryPath = "${PODS_ROOT}/UnityLayer"
end

Pod::Spec.new do |s|
  s.name             = 'UnityLayer'
  s.version          = '0.1.0'
  s.summary          = 'A short description of UnityLayer.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://github.com/Max Petrovsky/UnityLayer'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Max Petrovsky' => 'maksym.petrovskyi@games24x7.com' }
  s.source           = { :git => 'git@bitbucket.org:maksym_petrovskyi/unitytestpod.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'
  s.source_files = 'UnityLayer/Classes/**/*'

  s.xcconfig = {
    'FRAMEWORK_SEARCH_PATHS[arch=arm64]' =>'$(inherited) "' + $UnityLibraryPath + '/Libraries/iOS"',
    'FRAMEWORK_SEARCH_PATHS[arch=armv7]' => '$(inherited) "' + $UnityLibraryPath + '/Libraries/iOS"',
    'FRAMEWORK_SEARCH_PATHS[arch=x86_64]' => '$(inherited) "' +  $UnityLibraryPath + '/Libraries/iOSSimulator"',
    'FRAMEWORK_SEARCH_PATHS[arch=i386]' => '$(inherited) "' + $UnityLibraryPath + '/Libraries/iOSSimulator"'
  }

  s.script_phases = [
    {
      :name => 'Copy Framework',
      :script => 'sh $PODS_TARGET_SRCROOT/CopyFramework.sh',
      :execution_position => :after_compile
    }
  ]
   s.preserve_paths = 'Libraries/**/*', 'CopyFramework.sh'
#   s.resources = 'sources/iOS/Data'
   s.public_header_files = 'Classes/public/**/*.h'
   s.prefix_header_contents = '#import "ObjcExtensions.h"'+"\n@import UnityFramework;\n#import <UnityFramework/NativeCallProxy.h>"

end

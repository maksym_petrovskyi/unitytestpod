using UnityEditor;
using System.Collections.Generic;
using System;
// Put this memu command script into Assets/Editor/

class ExportTool
{
	private static void ExportXcodeProject (string path, bool isSimulator)
	{
		EditorUserBuildSettings.SwitchActiveBuildTarget (BuildTarget.iOS);

		EditorUserBuildSettings.symlinkLibraries = true;
		EditorUserBuildSettings.development = true;
		EditorUserBuildSettings.allowDebugging = true;
		PlayerSettings.iOS.sdkVersion = isSimulator ? iOSSdkVersion.SimulatorSDK : iOSSdkVersion.DeviceSDK;
		List<string> scenes = new List<string>();
		for (int i = 0; i < EditorBuildSettings.scenes.Length; i++)
		{
			if (EditorBuildSettings.scenes [i].enabled)
			{
				scenes.Add (EditorBuildSettings.scenes [i].path);
			}
		}

		BuildPipeline.BuildPlayer (scenes.ToArray (), path, BuildTarget.iOS, BuildOptions.None);
	}

	static void ExportXcodeProjectCLI() {
		var args = System.Environment.GetCommandLineArgs();
		string path = null;
		bool isSimulator = false;
		for (int i = 0; i < args.Length; i++)
		{
				 if (args[i] == "-simulator")
				 {
						 isSimulator = true;
				 }

				 if (args[i] == "-exportPath")
				 {
						 path = args[i + 1];
				 }
		}
		if (path is null) {
			throw new ArgumentNullException(paramName: nameof(path), message: "Path cannot be null");
		}
		ExportXcodeProject(path, isSimulator);
	}
}
